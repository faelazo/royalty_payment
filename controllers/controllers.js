'use strict'

var data = require('../data/data');

var views = 0;

var controller = {

    reset: function(req, res){

        data.studios.studios.map( it => it.viewings = 0);
        views = 0;

        return res.status(202).end();
    },

    viewing: function(req, res){
        
        if (req.body.episode == null) return res.status(500).send("ERROR: Episode not found!");

        let episode = data.episodes.episodes.find(epi => {return epi.id === req.body.episode});

        if (episode == null) return res.status(500).send("ERROR: Episode not found!");

        if (req.body.customer == null) return res.status(500).send("ERROR: Customer not found!");

        views++;

        data.studios.studios.map( it => {

            if (it.id == episode.rightsowner){
                it.viewings++;
            };

        });

        return res.status(202).end();
    },

    payments: function(req, res){

        var result = data.studios.studios.map( it => {

            return {
                "rightsownerId": it.id,
                "rightsowner": it.name,
                "royalty": it.payment,
                "viewings": it.viewings
            }

        });

        return res.status(200).send({result});
    },

    rightOwner: function(req, res){

        var guid = req.url.substring(String("/payments/").length, req.url.length);

        var studio = data.studios.studios.find( it => it.id == guid);

        if (studio == null) return res.status(404).send("ERROR: GUID not found!");
        
        var result = {
            "rightsowner": studio.name,
            "royalty": studio.payment,
            "viewings": studio.viewings
        };

        return res.status(200).send({result})
    }
};

module.exports = controller;