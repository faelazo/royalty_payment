'use strict'

var fs = require('fs');

var data = {
    episodes: JSON.parse(fs.readFileSync('./data/episodes.json')),
    studios: JSON.parse(fs.readFileSync('./data/studios.json'))
};

data.studios.studios.map( it => it.viewings = 0);

module.exports = data;