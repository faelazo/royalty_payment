'use strict'

var express = require('express');
var controller = require('../controllers/controllers');

var router = express.Router();

router.get('/', (req, res) => {res.send('<h1>Hola Heroku</h1>')})
router.post('/reset', controller.reset);
router.post('/viewing', controller.viewing);
router.get('/payments', controller.payments);
router.get('/payments/*', controller.rightOwner);

module.exports = router;
